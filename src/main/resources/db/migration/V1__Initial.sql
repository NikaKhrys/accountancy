drop table if exists transactions;

create table transactions(
	transaction_id serial primary key,
	person_id int,
	position varchar(30),
	currency varchar(5),
	value int,
	date date,
	purpose varchar(30)
);
    