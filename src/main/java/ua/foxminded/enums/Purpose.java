/* (C)2023-2024 */
package ua.foxminded.enums;

/**
 * Enum with all possible transactions purposes
 *
 * @author veronika_khrystoforova
 */
public enum Purpose {
  SCHOLARSHIP,
  SALARY,
  CONTRACT_PAYMENT;
}
