/* (C)2023-2024 */
package ua.foxminded.enums;

/**
 * Enum with all possible users positions
 *
 * @author veronika_khrystoforova
 */
public enum Position {
  STUDENT,
  PROFESSOR
}
