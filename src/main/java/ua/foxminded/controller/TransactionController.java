/* (C)2023-2024 */
package ua.foxminded.controller;

import java.time.LocalDate;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.foxminded.service.CurrencyService;
import ua.foxminded.service.TransactionService;
import ua.foxminded.service.dto.TransactionDto;

/**
 * A simple REST controller, which supports end points to manage users currency transactions
 *
 * @author veronika_khrystoforova
 */
@RestController
@RequestMapping("/transactions")
public class TransactionController {
  private final TransactionService transactionService;
  private final CurrencyService currencyService;

  @Autowired
  public TransactionController(
      TransactionService transactionService, CurrencyService currencyService) {
    this.transactionService = transactionService;
    this.currencyService = currencyService;
  }

  /**
   * Returns transactions page in given currency
   *
   * @param personId - id if the user whose transactions are requested
   * @param startDate - date from which transaction are requested
   * @param endDate - date to which transaction are requested
   * @param currency - currency in which transaction are requested
   * @return ResponseEntity<Page<TransactionDto>>
   */
  @GetMapping("/{id}")
  public ResponseEntity<Page<TransactionDto>> getTransactionsInCurrency(
      @PathVariable(name = "id") Integer personId,
      @ParameterObject Pageable pageable,
      @RequestParam(name = "startDate") LocalDate startDate,
      @RequestParam(name = "endDate") LocalDate endDate,
      @RequestParam(name = "currency") String currency) {
    Page<TransactionDto> transactions =
        transactionService.getPersonsTransactions(personId, startDate, endDate, pageable);
    if (transactions.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    Page<TransactionDto> transactionsInCurrency =
        currencyService.getTransactionsInCurrency(transactions, currency);
    return new ResponseEntity<>(transactionsInCurrency, HttpStatus.OK);
  }
}
