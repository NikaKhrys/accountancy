/* (C)2023 */
package ua.foxminded.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import java.time.LocalDate;
import lombok.Data;
import ua.foxminded.enums.Position;
import ua.foxminded.enums.Purpose;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionDto {

  private Integer id;

  private Integer personId;

  @Enumerated(EnumType.STRING)
  private Position position;

  private String currency;

  private Integer value;

  private LocalDate date;

  @Enumerated(EnumType.STRING)
  private Purpose purpose;
}
