/* (C)2023-2024 */
package ua.foxminded.service.impls;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import ua.foxminded.service.CurrencyService;
import ua.foxminded.service.dto.TransactionDto;

@Service
public class CurrencyServiceImpl implements CurrencyService {
  @Value("${bankApi}")
  private String baseUrl;

  private static final String URI = "/statdirectory/exchange";
  private static final String DATE_PATTERN = "yyyyMMdd";

  public Page<TransactionDto> getTransactionsInCurrency(
      Page<TransactionDto> transactions, String currency) {
    return transactions.map(transactionDto -> converseValue(transactionDto, currency));
  }

  private double getRate(String currency, String date) {
    Map<String, String> response =
        WebClient.builder()
            .baseUrl(baseUrl)
            .build()
            .get()
            .uri(
                uriBuilder ->
                    uriBuilder
                        .path(URI)
                        .queryParam("valcode", currency)
                        .queryParam("date", date)
                        .queryParam("json")
                        .build())
            .retrieve()
            .bodyToFlux(new ParameterizedTypeReference<Map<String, String>>() {})
            .blockFirst();
    if (response == null || response.get("rate") == null) {
      return -1;
    }
    return Double.parseDouble(response.get("rate"));
  }

  private TransactionDto converseValue(TransactionDto transaction, String currency) {
    if (Objects.equals(currency, "UAH")) {
      return transaction;
    }
    int newValue = calculateValueInCurrency(transaction.getValue(), currency);
    transaction.setValue(newValue);
    transaction.setCurrency(currency);
    return transaction;
  }

  private int calculateValueInCurrency(int valueInUah, String currency) {
    double rate = getRate(currency, parseCurrentTime());
    if (rate == -1) {
      return -1;
    }
    return (int) (valueInUah / rate);
  }

  private String parseCurrentTime() {
    long timeInMillis = System.currentTimeMillis();
    Date date = new Date(timeInMillis);
    SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
    return dateFormat.format(date);
  }
}
