/* (C)2023-2024 */
package ua.foxminded.service.impls;

import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ua.foxminded.mappers.TransactionMapper;
import ua.foxminded.repository.TransactionRepository;
import ua.foxminded.repository.entity.Transaction;
import ua.foxminded.service.TransactionService;
import ua.foxminded.service.dto.TransactionDto;

@Service
public class TransactionServiceImpl implements TransactionService {
  private final TransactionRepository repository;
  private final TransactionMapper mapper;

  @Autowired
  public TransactionServiceImpl(TransactionRepository repository, TransactionMapper mapper) {
    this.repository = repository;
    this.mapper = mapper;
  }

  public Page<TransactionDto> getPersonsTransactions(
      Integer personId, LocalDate startDate, LocalDate endDate, Pageable pageable) {
    Page<Transaction> transactions =
        repository.getPersonsTransactions(personId, startDate, endDate, pageable);
    return mapper.toDtos(transactions);
  }
}
