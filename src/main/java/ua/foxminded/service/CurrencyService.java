/* (C)2023-2024 */
package ua.foxminded.service;

import org.springframework.data.domain.Page;
import ua.foxminded.service.dto.TransactionDto;

/**
 * Service class that manages currency conversion
 *
 * @author veronika_khrystoforova
 */
public interface CurrencyService {

  /**
   * Returns transactions converted to given currency
   *
   * @param transactions - transactions to be converted
   * @param currency - currency to which transactions should be converted
   * @return Page<TransactionDto>
   */
  Page<TransactionDto> getTransactionsInCurrency(
      Page<TransactionDto> transactions, String currency);
}
