/* (C)2023-2024 */
package ua.foxminded.service;

import java.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ua.foxminded.service.dto.TransactionDto;

/**
 * Service class that manages Transactions
 *
 * @author veronika_khrystoforova
 */
public interface TransactionService {

  /**
   * Returns transactions in given date range and currency for user
   *
   * @param personId - id of the user whose transactions are requested
   * @param startDate - date from which transaction are requested
   * @param endDate - date to which transactions are requested
   * @param pageable - page size and number
   * @return Page<TransactionDto>
   */
  Page<TransactionDto> getPersonsTransactions(
      Integer personId, LocalDate startDate, LocalDate endDate, Pageable pageable);
}
