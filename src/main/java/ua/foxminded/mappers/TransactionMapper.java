/* (C)2023-2024 */
package ua.foxminded.mappers;

import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import ua.foxminded.repository.entity.Transaction;
import ua.foxminded.service.dto.TransactionDto;

/**
 * Mapper for Transaction entity/dto
 *
 * @author veronika_khrystoforova
 */
@Mapper(componentModel = "spring")
public interface TransactionMapper {
  TransactionDto toDto(Transaction transaction);

  default Page<TransactionDto> toDtos(Page<Transaction> transactions) {
    return transactions.map(this::toDto);
  }
}
