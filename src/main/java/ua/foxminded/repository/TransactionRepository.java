/* (C)2023-2024 */
package ua.foxminded.repository;

import java.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.foxminded.repository.entity.Transaction;

/**
 * Repository for Transaction entity
 *
 * @author veronika_khrystoforova
 */
@Repository
public interface TransactionRepository extends PagingAndSortingRepository<Transaction, Integer> {
  @Query(
      value =
          """
                    SELECT * FROM transactions
                    WHERE person_id = :id AND
                    date >= :startDate AND
                    date <= :endDate
                    """,
      nativeQuery = true)
  Page<Transaction> getPersonsTransactions(
      @Param("id") Integer personId,
      @Param("startDate") LocalDate startDate,
      @Param("endDate") LocalDate endDate,
      Pageable pageable);
}
