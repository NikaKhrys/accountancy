/* (C)2023 */
package ua.foxminded.repository.entity;

import jakarta.persistence.*;
import java.time.LocalDate;
import lombok.Data;
import ua.foxminded.enums.Position;
import ua.foxminded.enums.Purpose;

@Data
@Entity
@Table(name = "transactions")
public class Transaction {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "transaction_id")
  private Integer id;

  @Column(name = "person_id")
  private Integer personId;

  @Column(name = "position")
  @Enumerated(EnumType.STRING)
  private Position position;

  @Column(name = "currency")
  private String currency;

  @Column(name = "value")
  private Integer value;

  @Column(name = "date")
  private LocalDate date;

  @Column(name = "purpose")
  @Enumerated(EnumType.STRING)
  private Purpose purpose;
}
