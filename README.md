# Accountancy

### Project description
This is a web application that allows university to keep track of money transactions and convert currency with open bank api.  
Run this application as a part of [University](https://gitlab.com/NikaKhrys/university.git).  