FROM maven:3.8.4-openjdk-17 AS builder

WORKDIR /appAcc
COPY . /appAcc

RUN mvn clean install -DskipSpotlessApply=true

FROM openjdk:17
VOLUME /tmp
EXPOSE 8081

WORKDIR /appAcc
COPY --from=builder /appAcc/target/accountancy-1.0-SNAPSHOT.jar appAcc.jar
ENTRYPOINT ["java","-jar","appAcc.jar"]
